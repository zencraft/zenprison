package ru.zencraft.prison;

import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.UUID;

import static ru.zencraft.prison.Prison.dataFolder;
import static ru.zencraft.prison.Prison.server;

public class PrisonPlayer {
    public static DecimalFormat decimalFormat = new DecimalFormat("#.##");
    public static YamlConfiguration playersData = YamlConfiguration.loadConfiguration(new File(dataFolder, "players.yml"));
    public static Map<UUID, PrisonPlayer> playerMap = Maps.newHashMap();

    public Player player;
    public UUID uuid;
    public boolean autoSell = false;
    public Scoreboard scoreboard;
    private double money;
    private int blocksMined;
    private int level;
    private int kills;
    private int deaths;
    private double moneyBooster = 1;
    private int blockBooster = 1;
    private Prison.Faction faction = Prison.Faction.NONE;

    public PrisonPlayer(UUID uuid) {
        Player player = server.getPlayer(uuid);
        this.player = player;
        this.uuid = player.getUniqueId();
        this.money = playersData.getDouble(uuid.toString() + ".money");
        this.blocksMined = playersData.getInt(uuid.toString() + ".blocksMined");
        this.level = playersData.getInt(uuid.toString() + ".level", 1);
        this.kills = playersData.getInt(uuid.toString() + ".kills");
        this.deaths = playersData.getInt(uuid.toString() + ".deaths");
        this.faction = Prison.Faction.valueOf(playersData.getString(uuid + ".faction", "NONE"));
        this.autoSell = playersData.getBoolean(uuid.toString() + ".autoSell", false);
        this.scoreboard = craftScoreboard();
        setScoreboard();

        playerMap.put(this.uuid, this);
    }

    public PrisonPlayer(Player player) {
        this.player = player;
        this.uuid = player.getUniqueId();
        this.money = playersData.getDouble(uuid.toString() + ".money");
        this.blocksMined = playersData.getInt(uuid.toString() + ".blocksMined");
        this.level = playersData.getInt(uuid.toString() + ".level", 1);
        this.kills = playersData.getInt(uuid.toString() + ".kills");
        this.deaths = playersData.getInt(uuid.toString() + ".deaths");
        this.faction = Prison.Faction.valueOf(playersData.getString(uuid + ".faction", "NONE"));
        this.autoSell = playersData.getBoolean(uuid.toString() + ".autoSell", false);
        this.scoreboard = craftScoreboard();
        setScoreboard();

        playerMap.put(this.uuid, this);
    }

    public static void calculateDrops(Player player) {
        PlayerInventory inv = player.getInventory();
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack item = inv.getItem(i);
            if (item == null) continue;
            if (item.getItemMeta().getLore() == null || item.getItemMeta().getLore().isEmpty()) {
                ItemStack drop = item.clone();
                item.setAmount(0);
                player.getWorld().dropItemNaturally(player.getLocation(), drop);
            }
        }
    }

    public static PrisonPlayer get(Player player) {
        return playerMap.get(player.getUniqueId());
    }

    public double getMoney() {
        return this.money;
    }

    public void setMoney(double money) {
        Objective objective = scoreboard.getObjective("stats");
        scoreboard.resetScores("§b" + decimalFormat.format(this.money).replace(",", "."));
        objective.getScore("§b" + decimalFormat.format(money).replace(",", ".")).setScore(12);
        this.money = money;
    }

    public int getBlocksMined() {
        return this.blocksMined;
    }

    public void setBlocksMined(int blocksMined) {
        Objective objective = scoreboard.getObjective("stats");
        scoreboard.resetScores("§b" + this.blocksMined + " ");
        objective.getScore("§b" + blocksMined + " ").setScore(9);
        this.blocksMined = blocksMined;
    }

    public void addTotalMaterialBlockMined(Material material, int i) {
        playersData.set(uuid + ".materialBlocksMined." + material.name(), playersData.getInt(uuid + ".materialBlocksMined." + material.name()) + i);
    }

    public int getTotalMaterialBlockMined(Material material) {
        return playersData.getInt(uuid + ".materialBlocksMined." + material.name());
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        Objective objective = scoreboard.getObjective("stats");
        scoreboard.resetScores("§b" + this.level + "  ");
        objective.getScore("§b" + level + "  ").setScore(6);
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20 + (level * 2));
        this.level = level;
    }

    public int getKills() {
        return this.kills;
    }

    public void setKills(int kills) {
        Objective objective = scoreboard.getObjective("stats");
        scoreboard.resetScores("§b" + this.kills + "   ");
        objective.getScore("§b" + kills + "   ").setScore(3);
        this.kills = kills;
    }

    public int getDeaths() {
        return this.deaths;
    }

    public void setDeaths(int deaths) {
        Objective objective = scoreboard.getObjective("stats");
        scoreboard.resetScores("§b" + this.deaths + "    ");
        objective.getScore("§b" + deaths + "    ").setScore(0);
        this.deaths = deaths;
    }

    public Prison.Faction getFaction() {
        return this.faction;
    }

    public void setFaction(Prison.Faction faction) {
        String currentFaction = this.faction.name().toLowerCase();
        if (!currentFaction.equals("none")) {
            server.getScoreboardManager().getMainScoreboard().getTeam(currentFaction).removeEntry(this.player.getName());
        }
        this.faction = faction;
        if (this.faction != Prison.Faction.NONE)
            server.getScoreboardManager().getMainScoreboard().getTeam(faction.name().toLowerCase()).addEntry(this.player.getName());
    }

    public double getMoneyBooster() {
        return this.moneyBooster;
    }

    public void setMoneyBooster(double booster) {
        this.moneyBooster = booster;
    }

    public int getBlockBooster() {
        return this.blockBooster;
    }

    public void setBlockBooster(int booster) {
        this.blockBooster = booster;
    }

    public void save() {
        playersData.set(this.uuid.toString() + ".name", this.player.getName());
        playersData.set(this.uuid.toString() + ".money", this.money);
        playersData.set(this.uuid.toString() + ".blocksMined", this.blocksMined);
        playersData.set(this.uuid.toString() + ".level", this.level);
        playersData.set(this.uuid.toString() + ".kills", this.kills);
        playersData.set(this.uuid.toString() + ".deaths", this.deaths);
        playersData.set(this.uuid.toString() + ".autoSell", this.autoSell ? true : null);
        playersData.set(this.uuid.toString() + ".faction", this.faction != Prison.Faction.NONE ? faction.name() : null);
        playersData.set(this.uuid.toString() + ".moneyBooster", this.moneyBooster);
    }

    public void unload() {
        save();
        playerMap.remove(this.uuid);
    }

    public void setScoreboard() {
        if (player != null) player.setScoreboard(this.scoreboard);
    }

    public Scoreboard craftScoreboard() {
        Scoreboard scoreboard = this.scoreboard;
        if (scoreboard == null) scoreboard = server.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.getObjective("stats");
        if (objective == null) {
            objective = scoreboard.registerNewObjective("stats", "dummy");
            objective.setDisplayName("§cPrison");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        }

        objective.getScore("§cДеньги").setScore(13);
        objective.getScore("§b" + decimalFormat.format(this.money).replace(",", ".")).setScore(12);
        objective.getScore(" ").setScore(11);
        objective.getScore("§cДобыто блоков").setScore(10);
        objective.getScore("§b" + this.blocksMined + " ").setScore(9);
        objective.getScore("  ").setScore(8);
        objective.getScore("§cУровень").setScore(7);
        objective.getScore("§b" + this.level + "  ").setScore(6);
        objective.getScore("   ").setScore(5);
        objective.getScore("§cУбийства").setScore(4);
        objective.getScore("§b" + this.kills + "   ").setScore(3);
        objective.getScore("    ").setScore(2);
        objective.getScore("§cСмерти").setScore(1);
        objective.getScore("§b" + this.deaths + "    ").setScore(0);
        return scoreboard;
    }
}
