package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

import java.util.ArrayList;
import java.util.List;

import static ru.zencraft.prison.Prison.config;

public class LevelMenu extends CustomMenu {
    public static ItemStack levelUp = new ItemBuilder(Material.EXP_BOTTLE).displayName("§aПовысить уровень!").build();
    public static ItemStack[] defaultContents = null;

    PrisonPlayer prisonPlayer;

    public LevelMenu(PrisonPlayer prisonPlayer) {
        super(1, "§2Повышение уровня");
        this.prisonPlayer = prisonPlayer;
        initItems();
    }

    public static List<String> getInfo(PrisonPlayer prisonPlayer) {
        List<String> list = new ArrayList<>();
        int level = prisonPlayer.getLevel();
        double money = prisonPlayer.getMoney();
        int blocks = prisonPlayer.getBlocksMined();

        int needMoney = config.getInt("levels." + level + 1 + ".price");
        int needBlocks = config.getInt("levels." + level + 1 + ".blocks");
        list.add("§6Ваш уровень: §a" + level);
        list.add("§6Стоимость следующего уровня: " + (needMoney <= money ? "§a" + needMoney : "§c" + needMoney));
        list.add("§6Необходимо блоков: " + (needBlocks <= blocks ? "§a" + needBlocks : "§c" + needBlocks));
        return list;
    }

    public void initItems() {
        if (defaultContents == null) {
            ItemStack glass = new ItemBuilder(Material.STAINED_GLASS_PANE).displayName(" ").build();
            for (int i = 0; i < 9; i++) {
                inventory.setItem(i, glass);
            }
            inventory.setItem(3, levelUp);
            defaultContents = inventory.getContents();
            inventory.setItem(5, new ItemBuilder(Material.BOOK).displayName("§aИнформация").lore(getInfo(prisonPlayer)).build());
        } else {
            inventory.setContents(defaultContents);
            inventory.setItem(5, new ItemBuilder(Material.BOOK).displayName("§aИнформация").lore(getInfo(prisonPlayer)).build());
        }
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
        if (event.getClickedInventory() == null || event.getClickedInventory().getHolder() != inventory.getHolder())
            return;
        ItemStack currentItem = event.getCurrentItem();
        if (currentItem.getType() == Material.AIR) return;
        Player player = (Player) event.getWhoClicked();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        switch (currentItem.getType()) {
            case EXP_BOTTLE: {
                int level = prisonPlayer.getLevel();
                double money = prisonPlayer.getMoney();
                int blocks = prisonPlayer.getBlocksMined();

                int needMoney = config.getInt("levels." + level + 1 + ".price");
                int needBlocks = config.getInt("levels." + level + 1 + ".blocks");

                if (money >= needMoney && blocks >= needBlocks) {
                    prisonPlayer.setLevel(level + 1);
                    player.closeInventory();
                    player.getLocation().getWorld().playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                    player.sendTitle("§aПоздравляем!", "§aВы повысили свой уровень", 5, 60, 20);
                } else {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, Integer.MAX_VALUE, 0.5f);
                }
                break;
            }
        }
    }
}
