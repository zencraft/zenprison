package ru.zencraft.prison.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.PrisonPlayer;

import java.util.ArrayList;

public class AutosellCommand extends Command {
    public AutosellCommand() {
        super
                (
                        "autosell",
                        "Автопродажа",
                        "/autosell <on/off>",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            PrisonPlayer prsonPlayer = PrisonPlayer.get(player);
            if (args.length < 1) {
                sender.sendMessage(usageMessage);
                return true;
            }
            switch (args[0]) {
                case "on":
                case "true": {
                    prsonPlayer.autoSell = true;
                    player.sendMessage("§aАвтопродажа включена");
                    break;
                }
                case "false":
                case "off": {
                    prsonPlayer.autoSell = false;
                    player.sendMessage("§eАвтопродажа выключена");
                    break;
                }
            }
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}