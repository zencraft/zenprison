package ru.zencraft.prison.api;


import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import static ru.zencraft.prison.Prison.server;

public abstract class CustomMenu implements InventoryHolder {
    public Inventory inventory;

    public CustomMenu(int rows, String title) {
        inventory = server.createInventory(CustomMenu.this, rows * 9, title);
    }

    public abstract void onClick(InventoryClickEvent event);

    @Override
    public Inventory getInventory() {
        return inventory;
    }
}
