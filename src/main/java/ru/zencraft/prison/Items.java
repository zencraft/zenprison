package ru.zencraft.prison;

import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.api.ItemBuilder;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Items {
    public static Map<String, ItemBuilder> itemBuilerMap = Maps.newHashMap();
    public static Map<String, ItemStack> itemMap = Maps.newHashMap();
    public static Map<String, Set<String>> requirementsMap = Maps.newHashMap();
    public static YamlConfiguration itemsCfg = YamlConfiguration.loadConfiguration(new File(Prison.dataFolder, "items.yml"));

    public static ItemStack apple = new ItemStack(Material.APPLE, 16);

    public Items() {
        itemsCfg.getKeys(false).forEach(keys -> {
            itemBuilerMap.put(keys,
                    new ItemBuilder(Material.valueOf(itemsCfg.getString(keys + ".material")))
                            .displayName(itemsCfg.getString(keys + ".name"))
                            .lore(itemsCfg.getStringList(keys + ".lore"))
                            .unbreakable(true)
                            .localizedName(keys)
            );
            requirementsMap.put(keys, new HashSet<>());
        });
        itemsCfg.getKeys(true).forEach(keys -> {
            //axe_2.encantments.DIG_SPEED = 1
            if (keys.contains(".enchantments.")) {
                String[] enchantSplit = keys.split(".enchantments.");
                itemBuilerMap.get(enchantSplit[0]).enchant(Enchantment.getByName(enchantSplit[1]), itemsCfg.getInt(keys));
            }
            if (keys.contains(".requirements.blocks.")) {
                String[] split = keys.split(".requirements.blocks.");
                Set<String> set = requirementsMap.get(split[0]);
                set.add(split[1]);
            }
        });

        itemBuilerMap.forEach((string, itemBuilder) -> {
            itemMap.put(string, itemBuilder.build());
        });
        itemBuilerMap = null;
    }
}
