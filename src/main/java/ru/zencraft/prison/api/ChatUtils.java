package ru.zencraft.prison.api;

import com.destroystokyo.paper.Title;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.zencraft.prison.Prison.plugin;

public class ChatUtils {
    public static Set<Player> chatCooldown = Sets.newHashSet();

    public static BaseComponent generateChat(Player player, String message) {
        String uuid = player.getUniqueId().toString();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        if (!chatCooldown.contains(player)) {
            ChatColor color = getChatColor(player, message);
            message = craftMessage(message).replaceFirst("!", "");
            if (message.equals(" ")) {
                return null;
            }
            if (!player.isOp()) chatCooldown.add(player);
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> chatCooldown.remove(player), 30);
            TextComponent textComponent = new TextComponent();
            BaseComponent[] nick = new ComponentBuilder(player.getDisplayName())
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                            new ComponentBuilder(
                                    prisonPlayer.player.getDisplayName() +
                                            "\n§cУровень: §b" + prisonPlayer.getLevel() +
                                            "\n§cБлоков: §b" + prisonPlayer.getBlocksMined() +
                                            "\n§cУбийств: §b" + prisonPlayer.getKills() +
                                            "\n§cСмертей: §b" + prisonPlayer.getDeaths() +
                                            (prisonPlayer.getFaction() != Prison.Faction.NONE ? "\n§cФракция: " + Prison.Faction.getDisplayName(prisonPlayer.getFaction()) : "")
                            ).create()))
                    .create();
            textComponent.addExtra(nick[0]);
            textComponent.addExtra("§7: ");
            BaseComponent[] coloredMessage = new ComponentBuilder(message).color(color).create();
            textComponent.addExtra(coloredMessage[0]);
            return textComponent;
        } else {
            player.sendMessage("Вы слишком быстро пишите в чат.");
            return null;
        }
    }


    public static String craftMessage(String message) {
        StringBuilder stringBuilder = new StringBuilder("");
        for (String strings : message.split(" ")) {
            stringBuilder.append
                    (
                            !hasWeb(strings) && !hasIp(strings)
                                    ? strings
                                    : Strings.repeat("*", strings.length())
                    ).append(" ");
        }
        return stringBuilder.toString();
    }

    public static void broadcastMessage(String message, Iterable<Player> list) {
        list.forEach(players -> players.sendMessage(message));
    }

    public static void broadcastActionBar(String actionBar, Iterable<Player> list) {
        list.forEach(players -> players.sendActionBar(actionBar));
    }

    public static void broadcastTitle(Title title, Iterable<Player> list) {
        list.forEach(players -> players.sendTitle(title));
    }

    public static void broadcastTitle(String title, String subTitle, Iterable<Player> list) {
        list.forEach(players -> players.sendTitle(title, subTitle));
    }

    public static void broadcastTitle(String title, String subTitle, int fadeIn, int stay, int fadeOut, Iterable<Player> list) {
        list.forEach(players -> players.sendTitle(title, subTitle, fadeIn, stay, fadeOut));
    }

    public static boolean hasIp(String message) {
        Pattern ipPattern = Pattern.compile("((?<![0-9])(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[.,-:; ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[., ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[., ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}))(?![0-9]))");
        Matcher regexMatcher = ipPattern.matcher(message);
        while (regexMatcher.find()) {
            if (regexMatcher.group().length() != 0 && ipPattern.matcher(message).find())
                return true;
        }
        return false;
    }

    public static boolean hasWeb(String message) {
        Pattern webpattern = Pattern.compile("[-a-zA-Z0-9@:%_\\+.~#?&//=]{2,256}\\.[a-z]{2,4}\\b(\\/[-a-zA-Z0-9@:%_\\+~#?&//=]*)?");
        Matcher regexMatcherurl = webpattern.matcher(message);
        while (regexMatcherurl.find()) {
            String text = regexMatcherurl.group().trim().replaceAll("www.", "").replaceAll("http://", "").replaceAll("https://", "");
            if (regexMatcherurl.group().length() != 0 && text.length() != 0) {
                if (webpattern.matcher(message).find())
                    return true;
            }
        }
        return false;
    }

    public static List<Player> getPlayersInRange(int range, Location origin) {
        List<Player> players = new ArrayList<>();
        int frange = range * range;
        origin.getWorld().getPlayers().forEach((p) -> {
            double d = p.getLocation().distanceSquared(origin);
            if (d <= frange) {
                players.add(p);
            }
        });
        return players;
    }

    public static ChatColor getChatColor(Player player, String message) {
        if (player.hasPermission("zencraft.chat.helper")) {
            return ChatColor.GREEN;
        }
        if (message.startsWith("!")) {
            return ChatColor.WHITE;
        } else {
            return ChatColor.GRAY;
        }
    }
}
