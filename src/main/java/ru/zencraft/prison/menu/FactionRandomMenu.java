package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Scoreboard;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

import java.util.ArrayList;
import java.util.List;

import static ru.zencraft.prison.Prison.*;

public class FactionRandomMenu extends CustomMenu {
    public static ItemStack redstonetorch = new ItemStack(Material.REDSTONE_TORCH_ON);
    public static ItemStack red = new ItemBuilder(new ItemStack(Material.WOOL, 1, (short) 14)).displayName("§cФракция: §4Демоны").build();
    public static ItemStack blue = new ItemBuilder(new ItemStack(Material.WOOL, 1, (short) 9)).displayName("§cФракция: §3Ангелы").build();

    public static ItemStack[] defaultContents = null;
    public int schedulerId = 0;
    public int ticks = 0;
    public int item = 0;
    public double perticks = 0.5;

    PrisonPlayer prisonPlayer;

    public FactionRandomMenu(PrisonPlayer prisonPlayer) {
        super(3, "");
        this.prisonPlayer = prisonPlayer;
        initItems();
        prisonPlayer.player.openInventory(inventory);

        schedulerId = server.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            ticks++;
            rollMenu();
        }, 0, 1);
        server.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            server.getScheduler().cancelTask(schedulerId);
            server.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                prisonPlayer.player.closeInventory();
                switch (inventory.getItem(13).getDurability()) {
                    case 14: {
                        prisonPlayer.player.sendMessage("§aПоздравляю! Вы присоеденились к §4Демонам");
                        prisonPlayer.setFaction(Faction.RED);
                        break;
                    }
                    case 9: {
                        prisonPlayer.player.sendMessage("§aПоздравляю! Вы присоеденились к §3Ангелам");
                        prisonPlayer.setFaction(Faction.BLUE);
                        break;
                    }
                }
            }, 60);
        }, 250);
    }

    public void initItems() {
        if (defaultContents == null) {
            ItemStack glasswhite = new ItemBuilder(Material.STAINED_GLASS_PANE).displayName(" ").build();
            ItemStack glassblack = new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15)).displayName(" ").build();
            for (int i = 0; i < 9; i++) {
                inventory.setItem(i, glassblack);
            }
            for (int i = 18; i < 27; i++) {
                inventory.setItem(i, glassblack);
            }
            inventory.setItem(4, redstonetorch);
            inventory.setItem(22, redstonetorch);
            defaultContents = inventory.getContents();
        } else {
            inventory.setContents(defaultContents);
        }
    }

    public void rollMenu() {
        if (ticks > perticks) {
            perticks = perticks * 1.075;
            prisonPlayer.player.playSound(prisonPlayer.player.getLocation(), Sound.UI_BUTTON_CLICK, Integer.MAX_VALUE, 1);
            prisonPlayer.player.openInventory(inventory);
            List<ItemStack> list = new ArrayList<>();
            for (int i = 9; i < 18; i++) {
                list.add(inventory.getItem(i));
            }
            if (item == 81) {
                Scoreboard scoreboard = server.getScoreboardManager().getMainScoreboard();
                if (scoreboard.getTeam("blue").getSize() > scoreboard.getTeam("red").getSize()) list.add(red);
                else list.add(blue);
            } else list.add(random.nextBoolean() ? red : blue);
            item++;
            int slot = 9;
            for (int i = 1; i < list.size(); i++) {
                inventory.setItem(slot, list.get(i));
                slot++;
            }
            list = null;
        }
    }


    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }
}
