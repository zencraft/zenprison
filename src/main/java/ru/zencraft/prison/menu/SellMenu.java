package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.commands.SellCommand;

public class SellMenu extends CustomMenu {
    Player player;

    public SellMenu(Player player) {
        super(3, "§2Продажа");
        this.player = player;
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursorItem = event.getCursor();
        if (currentItem == null || cursorItem == null) return;

        if (currentItem.getType() != Material.AIR && SellCommand.sellMap.get(currentItem.getType()) == null) {
            event.setCancelled(true);
        }
        if (cursorItem.getType() != Material.AIR && SellCommand.sellMap.get(cursorItem.getType()) == null) {
            event.setCancelled(true);
        }
    }

}
