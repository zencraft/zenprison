package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

public class FactionChoseMenu extends CustomMenu {
    public static ItemStack[] defaultContents = null;
    public static ItemStack red = new ItemBuilder(new ItemStack(Material.WOOL, 1, (short) 14)).displayName("§cФракция: §4Демоны").build();
    public static ItemStack blue = new ItemBuilder(new ItemStack(Material.WOOL, 1, (short) 9)).displayName("§cФракция: §3Ангелы").build();

    Player player;

    public FactionChoseMenu(Player player) {
        super(3, "");
        this.player = player;
        initItems();
        player.openInventory(inventory);
    }

    public void initItems() {
        if (defaultContents == null) {
            inventory.setItem(12, red);
            inventory.setItem(14, blue);
            defaultContents = inventory.getContents();

        } else {
            inventory.setContents(defaultContents);
        }
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
        if (event.getClickedInventory() == null || event.getClickedInventory().getHolder() != inventory.getHolder())
            return;
        ItemStack currentItem = event.getCurrentItem();
        if (currentItem.getType() == Material.AIR) return;
        Player player = (Player) event.getWhoClicked();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        switch (currentItem.getDurability()) {
            case 14: {
                prisonPlayer.player.sendMessage("§aПоздравляю! Вы присоеденились к §4Демонам");
                prisonPlayer.setFaction(Prison.Faction.RED);
                player.closeInventory();
                break;
            }
            case 9: {
                prisonPlayer.player.sendMessage("§aПоздравляю! Вы присоеденились к §3Ангелам");
                prisonPlayer.setFaction(Prison.Faction.BLUE);
                player.closeInventory();
                break;
            }
        }
    }
}