package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.Bosses.Boss;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

import java.util.Arrays;

import static ru.zencraft.prison.Prison.plugin;
import static ru.zencraft.prison.Prison.server;

public class BossMenu extends CustomMenu {
    public BossMenu() {
        super(3, "Боссы");
        server.getScheduler().runTaskTimerAsynchronously(plugin, this::updateItems, 0, 20);
    }

    private void updateItems() {
        inventory.clear();
        Boss.bossMap.forEach((id, boss) -> {
            ItemBuilder ib;
            switch (boss.type) {
                case ZOMBIE: {
                    ib = new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (short) 2));
                    break;
                }
                case SKELETON: {
                    ib = new ItemBuilder(new ItemStack(Material.SKULL_ITEM));
                    break;
                }
                case WITHER_SKELETON: {
                    ib = new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (short) 1));
                    break;
                }
                case SPIDER: {
                    ib = new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (short) 3)).head("MHF_Spider");
                    break;
                }
                default: {
                    ib = new ItemBuilder(Material.DEAD_BUSH);
                }
            }
            ib.displayName(boss.displayName);
            if (boss.entity.isDead()) {
                ib.lore(Arrays.asList("§cСостояние : §cМёртвый", "§bВремя до появления: §b§l" + convertTime(boss.spawnCooldown)));
            } else {
                ib.lore(Arrays.asList("§cСостояние : §aЖивой"));
            }
            inventory.addItem(ib.build());
        });
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    public String convertTime(int seconds) {
        // total seconds
// calculate seconds
        int s = seconds % 60;
// add leading zero to seconds if needed
        s = s < 10 ? s : s;

// calculate minutes
        int m = (int) (Math.floor(seconds / 60) % 60);
// add leading zero to minutes if needed
        m = m < 10 ? m : m;

        return m + ":" + s;
    }
}
