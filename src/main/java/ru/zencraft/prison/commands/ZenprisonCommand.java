package ru.zencraft.prison.commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;

import java.util.ArrayList;
import java.util.UUID;

import static ru.zencraft.prison.Prison.server;
import static ru.zencraft.prison.PrisonPlayer.playersData;

public class ZenprisonCommand extends Command {
    private static String unknownCommand = "Unknown command. Type \"/help\" for help";

    public ZenprisonCommand() {
        super
                (
                        "zenprison",
                        "Основная команда ZenPrison",
                        "/zenprison",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(unknownCommand);
            return true;
        }
        switch (args[0]) {
            case "info": {
                if (args.length == 2) {
                    UUID uuid = getUUID(args[1]);
                    Player player = server.getPlayer(uuid);
                    if (player != null && player.isOnline()) {
                        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                        sender.sendMessage("§cНик: §b" + player.getName());
                        sender.sendMessage("§cДеньги: §b" + prisonPlayer.getMoney());
                        sender.sendMessage("§cБлоки: §b" + prisonPlayer.getBlocksMined());
                        sender.sendMessage("§cУровень: §b" + prisonPlayer.getLevel());
                        sender.sendMessage("§cУбийства: §b" + prisonPlayer.getKills());
                        sender.sendMessage("§cСмерти: §b" + prisonPlayer.getDeaths());
                        sender.sendMessage("§cФракция: §b" + prisonPlayer.getFaction().name());
                    } else if (uuid != null) {
                        sender.sendMessage("§cДеньги: §b" + playersData.getDouble(uuid.toString() + ".money"));
                        sender.sendMessage("§cБлоки: §b" + playersData.getInt(uuid.toString() + ".blocksMined"));
                        sender.sendMessage("§cУровень: §b" + playersData.getInt(uuid.toString() + ".level", 1));
                        sender.sendMessage("§cУбийства: §b" + playersData.getInt(uuid.toString() + ".kills"));
                        sender.sendMessage("§cСмерти: §b" + playersData.getInt(uuid.toString() + ".deaths"));
                        sender.sendMessage("§cФракция: §b" + playersData.getString(uuid.toString() + ".faction", "NONE"));
                    } else {
                        sender.sendMessage("Игрок не заходил на сервер");
                    }
                }
                break;
            }
            case "eco":
            case "money": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int money = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer.get(player).setMoney(money);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + money + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(uuid.toString() + ".money", money);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + money + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    case "add": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int money = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                                prisonPlayer.setMoney(prisonPlayer.getMoney() + money);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + money + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(
                                        uuid.toString() + "." + args[0],
                                        playersData.getInt(uuid.toString() + ".money") + money);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + money + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }
            case "blocks":
            case "block": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int blocks = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer.get(player).setBlocksMined(blocks);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + blocks + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(uuid.toString() + ".blocksMined", blocks);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + blocks + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    case "add": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int blocks = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                                prisonPlayer.setBlocksMined(prisonPlayer.getBlocksMined() + blocks);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + blocks + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(
                                        uuid.toString() + "." + args[0],
                                        playersData.getInt(uuid.toString() + ".blocksMined") + blocks);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + blocks + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }

            case "lvl":
            case "level":
            case "levels": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int level = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer.get(player).setLevel(level);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + level + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(uuid.toString() + ".level", level);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + level + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    case "add": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int level = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                                prisonPlayer.setLevel(prisonPlayer.getLevel() + level);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + level + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(
                                        uuid.toString() + "." + args[0],
                                        playersData.getInt(uuid.toString() + ".level") + level);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + level + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }

            case "kills": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int kills = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer.get(player).setKills(kills);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + kills + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(uuid.toString() + ".kills", kills);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + kills + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    case "add": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int kills = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                                prisonPlayer.setKills(prisonPlayer.getKills() + kills);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + kills + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(
                                        uuid.toString() + "." + args[0],
                                        playersData.getInt(uuid.toString() + ".kills") + kills);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + kills + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }

            case "deaths": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int deaths = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer.get(player).setDeaths(deaths);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + deaths + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(uuid.toString() + ".deaths", deaths);
                                sender.sendMessage("§aУстановленно " + args[0] + " " + deaths + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    case "add": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int deaths = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                                prisonPlayer.setDeaths(prisonPlayer.getDeaths() + deaths);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + deaths + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                playersData.set(
                                        uuid.toString() + "." + args[0],
                                        playersData.getInt(uuid.toString() + ".deaths") + deaths);
                                sender.sendMessage("§aДобавленно " + args[0] + " " + deaths + " UUID " + uuid);
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }
            case "booster": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "global": {
                        if (args.length == 2) {
                            sender.sendMessage(unknownCommand);
                            return true;
                        }
                        switch (args[2]) {
                            case "blocks": {
                                Player player = server.getPlayer(args[3]);
                                if (player == null) {
                                    sender.sendMessage("Игрок не на сервере");
                                    return true;
                                }
                                int booster = Integer.parseInt(args[4].replace("x", ""));
                                int minutes = Integer.parseInt(args[5]);
                                Prison.setBlockGlobalBooster(booster, minutes, player);
                                break;
                            }
                            case "money": {
                                Player player = server.getPlayer(args[3]);
                                if (player == null) {
                                    sender.sendMessage("Игрок не на сервере");
                                    return true;
                                }
                                double booster = Double.parseDouble(args[4].replace("x", ""));
                                int minutes = Integer.parseInt(args[5]);
                                Prison.setMoneyGlobalBooster(booster, minutes, player);
                                break;
                            }
                        }
                        break;
                    }
                    case "single":
                    case "personal": {
                        if (args.length == 2) {
                            sender.sendMessage(unknownCommand);
                            return true;
                        }
                        switch (args[2]) {
                            case "blocks": {
                                Player player = server.getPlayer(args[3]);
                                if (player == null) {
                                    sender.sendMessage("Игрок не на сервере");
                                    return true;
                                }
                                int booster = Integer.parseInt(args[4].replace("x", ""));
                                int minutes = Integer.parseInt(args[5]);
                                Prison.setBlockPersonalBooster(booster, minutes, player);

                                break;
                            }
                            case "money": {
                                Player player = server.getPlayer(args[3]);
                                if (player == null) {
                                    sender.sendMessage("Игрок не на сервере");
                                    return true;
                                }
                                double booster = Double.parseDouble(args[4].replace("x", ""));
                                int minutes = Integer.parseInt(args[5]);
                                Prison.setMoneyPersonalBooster(booster, minutes, player);
                                break;
                            }
                        }
                        break;
                    }
                    default:
                        sender.sendMessage(unknownCommand);
                }
                break;
            }
            case "faction": {
                if (args.length == 1) {
                    sender.sendMessage(unknownCommand);
                    return true;
                }
                switch (args[1]) {
                    case "set": {
                        if (args.length == 4) {
                            UUID uuid = getUUID(args[2]);
                            int faction = Integer.parseInt(args[3]);
                            Player player = server.getPlayer(uuid);
                            if (player != null && player.isOnline()) {
                                switch (faction) {
                                    case 1: {
                                        PrisonPlayer.get(player).setFaction(Prison.Faction.BLUE);
                                        break;
                                    }
                                    case 2: {
                                        PrisonPlayer.get(player).setFaction(Prison.Faction.RED);
                                        break;
                                    }
                                    default: {
                                        PrisonPlayer.get(player).setFaction(Prison.Faction.NONE);
                                        player.getScoreboardTags().remove("leaveFaction");
                                    }
                                }
                                sender.sendMessage("§aУстановленно " + args[0] + " " + faction + " игроку " + player.getDisplayName());
                            } else if (uuid != null) {
                                sender.sendMessage("Игрок не онлайн");
                            } else {
                                sender.sendMessage("Игрок не заходил на сервер");
                            }
                        } else {
                            sender.sendMessage(unknownCommand);
                        }
                        break;
                    }
                    default: {
                        sender.sendMessage(unknownCommand);
                    }
                }
                break;
            }
            default: {
                sender.sendMessage(unknownCommand);
            }
        }

        return true;
    }

    @SuppressWarnings("deprication")
    private UUID getUUID(String playerName) {
        OfflinePlayer offlinePlayer = server.getOfflinePlayer(playerName);
        return offlinePlayer.getUniqueId();
    }
}
