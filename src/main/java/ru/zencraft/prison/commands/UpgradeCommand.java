package ru.zencraft.prison.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.Items;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.menu.UpgradeMenu;

import java.util.ArrayList;

public class UpgradeCommand extends Command {
    public UpgradeCommand() {
        super
                (
                        "upgrade",
                        "Улучшение предмета",
                        "/upgrade",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            if (!sender.isOp()) return true;
            Player player = (Player) sender;
            PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
            ItemStack itemStack = player.getInventory().getItemInMainHand();
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                player.sendMessage("Возьмите предмет в руку");
                return true;
            } else if (itemStack.getItemMeta().getLocalizedName() != null) {
                String id = itemStack.getItemMeta().getLocalizedName();
                ItemStack needUpgrade = Items.itemMap.get(id);
                if (needUpgrade == null) {
                    player.sendMessage("§сДанный предмет нельзя улучшить!");
                    return true;
                }
                ItemStack getUpgrade = Items.itemMap.get(Items.itemsCfg.getString(id + ".next"));
                if (getUpgrade == null) {
                    player.sendMessage("§сДанный предмет нельзя улучшить!");
                    return true;
                }
                player.openInventory(new UpgradeMenu(prisonPlayer, needUpgrade, getUpgrade).inventory);
            }
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}
