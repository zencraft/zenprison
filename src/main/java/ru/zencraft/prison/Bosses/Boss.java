package ru.zencraft.prison.Bosses;

import com.google.common.collect.Maps;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.menu.BossMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ru.zencraft.prison.Prison.*;

public class Boss {
    public static Map<String, Boss> bossMap = Maps.newHashMap();
    public static Vector zeroVelocity = new Vector();
    public static BossMenu bossMenu = new BossMenu();

    public YamlConfiguration config;

    public Map<Player, Double> damageMap = Maps.newHashMap();

    public String id;
    public LivingEntity entity;
    public Location spawnLocation;
    public String displayName;
    public int spawnCooldown;
    public double radius;
    public double bank;
    public ItemStack[] loot;
    public EntityType type;
    public double health;
    public double damage;
    public double distance;
    public BossBar bossBar;
    public BossAbility bossAbility = BossAbility.KNOCKBACK;
    public int currentAbilityCooldown = 10;

    int tick = 0;

    public Boss(String id) {
        this.id = id;
        createNewBoss();
        this.entity = craftBoss();
        bossMap.put(id, this);
    }

    public void createNewBoss() {
        config = YamlConfiguration.loadConfiguration(new File(dataFolder, "bosses/" + id + ".yml"));
        damageMap.clear();
        spawnLocation = new Location(Prison.world, config.getDouble("spawnLocation.x", 0.5), config.getDouble("spawnLocation.y", 70), config.getDouble("spawnLocation.z", 0.5));
        displayName = config.getString("displayName", "Босс");
        spawnCooldown = config.getInt("spawnCooldown", 30);
        bank = config.getDouble("bank", 100);
        type = EntityType.valueOf(config.getString("type", "zombie").toUpperCase());
        health = config.getDouble("health", 250);
        damage = config.getDouble("damage", 8);
        radius = config.getDouble("radius", 15);
        bossBar = server.createBossBar(displayName, BarColor.BLUE, BarStyle.SOLID);
    }

    public LivingEntity craftBoss() {
        LivingEntity entity = (LivingEntity) spawnLocation.getWorld().spawnEntity(spawnLocation, type);
        entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(damage);
        entity.setHealth(health);
        entity.setCustomNameVisible(true);
        entity.setCustomName(displayName);
        entity.getScoreboardTags().add("ZenPrison_boss");
        entity.getScoreboardTags().add("ZenPrison_boss#" + id);
        switch (type) {
            case ZOMBIE: {
                Zombie zombie = (Zombie) entity;
                zombie.setBaby(false);
                zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
                break;
            }
            default: {
                entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
            }
        }
        return entity;
    }

    public double getHealthPercentage() {
        return health / 100 * entity.getHealth();
    }

    public void updateBar() {
        double bossHealt = 1 / health * entity.getHealth();
        double finalProgress = bossBar.getProgress() - 0.0075;
        if (entity.getHealth() <= 0) {
            bossBar.removeAll();
        } else {
            if (finalProgress > bossHealt) bossBar.setProgress(finalProgress);
        }
        if (bossBar.getProgress() >= 0.5) bossBar.setColor(BarColor.GREEN);
        if (bossBar.getProgress() >= 0.25 && bossBar.getProgress() < 0.5) bossBar.setColor(BarColor.YELLOW);
        if (bossBar.getProgress() < 0.25) bossBar.setColor(BarColor.RED);
    }

    public void tick() {
        if (tick > 20) {
            tick = 0;
            tickSecond();
        }
        updateBar();
        //entity.setVelocity(zeroVelocity);
        entity.setFireTicks(0);
        tick++;
    }

    public void tickSecond() {
        distance = spawnLocation.distance(entity.getLocation());
        if (distance > radius) entity.teleport(spawnLocation);

        world.getPlayers().forEach(players -> {
            double playerDistance = spawnLocation.distance(players.getLocation());
            if (playerDistance > radius * 1.5) {
                bossBar.removePlayer(players);
            } else {
                players.sendActionBar("Дистанция :" + distance + " " + entity.getHealth() + "/" + health + " Респаун: " + spawnCooldown);
                if (!entity.isDead()) bossBar.addPlayer(players);
            }
        });

        if (currentAbilityCooldown == 0) {
            //runAbility();
            currentAbilityCooldown = 10;
        } else {
            currentAbilityCooldown--;
        }
        if (entity.isDead()) spawnCooldown--;
        if (spawnCooldown == 0) {
            createNewBoss();
            this.entity = craftBoss();
        }
    }

    public List<Player> getNearestPlayers(Location location, int radius) {
        List<Player> list = new ArrayList<>();
        world.getPlayers().forEach(players -> {
            double playerDistance = location.distance(players.getLocation());
            if (playerDistance <= radius) list.add(players);
        });
        return list;
    }

    public void runAbility(Player player, BossAbility ability) {
        if (entity.isDead()) return;
        if (ability == null) return;
        switch (ability) {
            case KNOCKBACK: {
                player.setVelocity(player.getLocation().getDirection().multiply(-config.getDouble("ability.knockback", 2)).setY(1));
                break;
            }
            case LIGHTNING: {
                player.getWorld().strikeLightning(player.getLocation());
                break;
            }
            case BLINDNESS: {
                player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, config.getInt("ability.blindness.duration", 5) * 20, config.getInt("ability.blindness.amplifier", 0)));
                break;
            }
            case SLOW: {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, config.getInt("ability.slow.duration", 5) * 20, config.getInt("ability.slow.amplifier", 2)));
                break;
            }
            case POISON: {
                player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, config.getInt("ability.poison.duration", 5) * 20, config.getInt("ability.poison.amplifier", 0)));
                break;
            }
        }
    }

    public enum BossAbility {
        KNOCKBACK, LIGHTNING, BLINDNESS, SLOW, POISON
    }
}
