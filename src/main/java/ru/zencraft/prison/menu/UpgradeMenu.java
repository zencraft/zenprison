package ru.zencraft.prison.menu;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.Items;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

import java.util.List;

import static ru.zencraft.prison.Prison.formatDouble;

public class UpgradeMenu extends CustomMenu {
    public static ItemStack[] defaultContents = null;
    public static ItemStack glass = new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 9)).displayName(" ").build();

    public ItemStack needUpgrade;
    public ItemStack getUpgrade;
    public PrisonPlayer prisonPlayer;

    public UpgradeMenu(PrisonPlayer prisonPlayer, ItemStack needUpgrade, ItemStack getUpgrade) {
        super(1, "§2Улучшение предмета");
        this.needUpgrade = needUpgrade.clone();
        this.getUpgrade = getUpgrade.clone();
        this.prisonPlayer = prisonPlayer;
        initItems();
    }

    private void initItems() {
        if (defaultContents == null) {
            for (int i = 0; i < 9; i++) {
                inventory.setItem(i, glass);
            }
            defaultContents = inventory.getContents();
            inventory.setItem(2, needUpgrade);
            inventory.setItem(6, getInfo(getUpgrade));
        } else {
            inventory.setContents(defaultContents);
            inventory.setItem(2, needUpgrade);
            inventory.setItem(6, getInfo(getUpgrade));
        }
    }

    private ItemStack getInfo(ItemStack itemStack) {
        String id = needUpgrade.getItemMeta().getLocalizedName();
        ItemBuilder itemBuilder = new ItemBuilder(itemStack);
        List<String> list = itemStack.getItemMeta().getLore();
        double money = prisonPlayer.getMoney();
        double moneyNeed = Items.itemsCfg.getDouble(id + ".price");
        list.add("§6Цена: " + (money >= moneyNeed ? "§a" + formatDouble(moneyNeed) : "§c" + formatDouble(moneyNeed)) + "$");
        list.add("§6Необходимо блоков:");
        Items.requirementsMap.get(id).forEach(materials -> {
            Material material = Material.valueOf(materials);
            int mined = prisonPlayer.getTotalMaterialBlockMined(material);
            int minedNeed = Items.itemsCfg.getInt(id + ".requirements.blocks." + materials);
            list.add("§6" + materials + ": " + (mined >= minedNeed ? "§a" + mined + "/" + minedNeed : "§c" + mined + "/" + minedNeed));
        });
        itemBuilder.lore(list);
        return itemBuilder.build();
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
        if (event.getClickedInventory() == null || event.getClickedInventory().getHolder() != inventory.getHolder())
            return;
        ItemStack currentItem = event.getCurrentItem();
        if (currentItem.getType() == Material.AIR) return;
        Player player = (Player) event.getWhoClicked();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        if (event.getSlot() == 6) {
            if (isRequirementComplete(currentItem)) {
                double money = prisonPlayer.getMoney();
                double moneyNeed = Items.itemsCfg.getDouble(currentItem.getItemMeta().getLocalizedName() + ".price");
                prisonPlayer.setMoney(money - moneyNeed);
                player.closeInventory();
                player.getInventory().setItemInMainHand(Items.itemMap.get(getUpgrade.getItemMeta().getLocalizedName()));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Integer.MAX_VALUE, 1);
            } else {
                player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, Integer.MAX_VALUE, 0.5f);
            }
        }
    }

    public boolean isRequirementComplete(ItemStack itemStack) {
        List<String> list = itemStack.getItemMeta().getLore();
        for (String string : list) {
            if (string.contains("§c")) return false;
        }
        return true;
    }
}
