package ru.zencraft.prison;

import com.google.common.collect.Maps;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import ru.zencraft.prison.Bosses.Boss;
import ru.zencraft.prison.Bosses.Rats;
import ru.zencraft.prison.commands.*;
import ru.zencraft.prison.menu.NPCShopMenu;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public final class Prison extends JavaPlugin {
    public static DecimalFormat decimalFormat = new DecimalFormat("#.##");
    public static Random random = new Random();
    public static double moneyGlobalBooster = 1;
    public static Map<UUID, Double> moneyPersonalBooster = Maps.newHashMap();
    public static int blockGlobalBooster = 1;
    public static Map<UUID, Integer> blockPersonalBooster = Maps.newHashMap();

    public static Server server;
    public static Plugin plugin;
    public static File dataFolder;
    public static YamlConfiguration config;
    public static World world;

    public static double calculateMoneyBooster(PrisonPlayer prisonPlayer) {
        double globalBooster = Prison.moneyGlobalBooster;
        double personalBooster = Prison.moneyPersonalBooster.getOrDefault(prisonPlayer.uuid, 1d);
        double permissionBooster = prisonPlayer.getMoneyBooster();

        return Math.max(Math.max(globalBooster, personalBooster), permissionBooster);
    }

    public static int calculateBlockBooster(PrisonPlayer prisonPlayer) {
        int globalBooster = Prison.blockGlobalBooster;
        int personalBooster = Prison.blockPersonalBooster.getOrDefault(prisonPlayer.uuid, 1);
        int permissionBooster = prisonPlayer.getBlockBooster();

        return Math.max(Math.max(globalBooster, personalBooster), permissionBooster);
    }

    public static void setMoneyGlobalBooster(double booster, int minutes, Player player) {
        moneyGlobalBooster = booster;
        server.broadcastMessage("§6" + player.getName() + " §eактивировал x" + booster + " бустер денег на " + minutes + " минут(ы)");
        server.getScheduler().runTaskLaterAsynchronously(plugin, () -> moneyGlobalBooster = 1, 20 * 60 * minutes);
    }

    public static void setMoneyPersonalBooster(double booster, int minutes, Player player) {
        UUID uuid = player.getUniqueId();
        moneyPersonalBooster.put(uuid, booster);
        player.sendMessage("§eВы получили x" + booster + " бустер денег на " + minutes + " минут(ы)");
        server.getScheduler().runTaskLaterAsynchronously(plugin, () -> moneyPersonalBooster.remove(uuid), 20 * 60 * minutes);
    }

    public static void setBlockGlobalBooster(int booster, int minutes, Player player) {
        blockGlobalBooster = booster;
        server.broadcastMessage("§6" + player.getName() + " §eактивировал x" + booster + " бустер блоков на " + minutes + " минут(ы)");
        server.getScheduler().runTaskLaterAsynchronously(plugin, () -> moneyGlobalBooster = 1, 20 * 60 * minutes);
    }

    public static void setBlockPersonalBooster(int booster, int minutes, Player player) {
        player.sendMessage("§eВы получили x" + booster + " бустер блоков на " + minutes + " минут(ы)");
        UUID uuid = player.getUniqueId();
        blockPersonalBooster.put(uuid, booster);
        server.getScheduler().runTaskLaterAsynchronously(plugin, () -> moneyPersonalBooster.remove(uuid), 20 * 60 * minutes);
    }

    public static String formatDouble(double d) {
        return decimalFormat.format(d).replace(",", ".");
    }

    @Override
    public void onDisable() {
        server.getOnlinePlayers().forEach(players -> {
            PrisonPlayer prisonPlayer = PrisonPlayer.get(players);
            prisonPlayer.unload();
        });
        try {
            PrisonPlayer.playersData.save(new File(dataFolder, "players.yml"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        NPCShopMenu.menuMap.forEach((key, value) -> {
            try {
                value.cfg.save(new File(dataFolder, "shops/" + key + ".yml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Boss.bossMap.forEach((id, boss) -> {
            boss.bossBar.removeAll();
            boss.entity.remove();
        });
        Rats.removeAll();
    }

    @Override
    public void onEnable() {
        server = getServer();
        plugin = this;
        dataFolder = getDataFolder();
        world = server.getWorlds().get(0);

        config = YamlConfiguration.loadConfiguration(new File(dataFolder, "config.yml"));


        server.getPluginManager().registerEvents(new MainListener(), this);

        new Items();
        loadShops();
        loadBosses();

        server.getCommandMap().register("zenprison", new LevelCommand());
        server.getCommandMap().register("zenprison", new ZenprisonCommand());
        server.getCommandMap().register("zenprison", new SellCommand());
        server.getCommandMap().register("zenprison", new AutosellCommand());
        server.getCommandMap().register("zenprison", new UnbreakableCommand());
        server.getCommandMap().register("zenprison", new GiftCommand());
        server.getCommandMap().register("zenprison", new FactionCommand());
        server.getCommandMap().register("zenprison", new UpgradeCommand());
        server.getCommandMap().register("zenprison", new PayCommand());
        server.getCommandMap().register("zenprison", new NpcCommand());
        server.getCommandMap().register("zenprison", new BossCommand());

        registerTeams();
        server.getOnlinePlayers().forEach(players -> {
            new PrisonPlayer(players);
        });

        server.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            server.getOnlinePlayers().forEach(players -> PrisonPlayer.get(players).setScoreboard());
        }, 0, 5);

        server.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            Boss.bossMap.forEach((id, boss) -> {
                boss.tick();
            });
        }, 0, 1);
        new Rats();
    }

    private void registerTeams() {
        Scoreboard scoreboard = server.getScoreboardManager().getMainScoreboard();
        Team blue = scoreboard.getTeam("blue");
        if (blue == null) {
            blue = scoreboard.registerNewTeam("blue");
            blue.setColor(ChatColor.DARK_AQUA);
            blue.setCanSeeFriendlyInvisibles(true);
            blue.setAllowFriendlyFire(false);
            blue.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.FOR_OTHER_TEAMS);
            blue.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.FOR_OWN_TEAM);
        }

        Team red = scoreboard.getTeam("red");
        if (red == null) {
            blue = scoreboard.registerNewTeam("red");
            blue.setColor(ChatColor.RED);
            blue.setCanSeeFriendlyInvisibles(true);
            blue.setAllowFriendlyFire(false);
            blue.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.FOR_OTHER_TEAMS);
            blue.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.FOR_OWN_TEAM);
        }
    }

    private void loadShops() {
        File dir = new File(dataFolder, "shops");
        if (!dir.exists()) dir.mkdir();
        for (File file : dir.listFiles()) {
            String id = file.getName().replace(".yml", "");
            new NPCShopMenu(id);
        }
    }

    private void loadBosses() {
        File dir = new File(dataFolder, "bosses");
        if (!dir.exists()) dir.mkdir();
        for (File file : dir.listFiles()) {
            String id = file.getName().replace(".yml", "");
            new Boss(id);
        }
    }

    public enum Faction {
        RED, BLUE, NONE;

        public static String getDisplayName(Faction faction) {
            switch (faction) {
                case RED: {
                    return "§4Демоны";
                }
                case BLUE: {
                    return "§3Ангелы";
                }
                default: {
                    return null;
                }
            }
        }
    }
}
