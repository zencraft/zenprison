package ru.zencraft.prison.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.menu.FactionChoseMenu;
import ru.zencraft.prison.menu.FactionRandomMenu;

import java.util.ArrayList;

public class FactionCommand extends Command {
    public FactionCommand() {
        super
                (
                        "faction",
                        "Выбор фракции",
                        "/faction",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            PrisonPlayer prsonPlayer = PrisonPlayer.get(player);
            if (args.length == 0) {
                if (prsonPlayer.getFaction() == Prison.Faction.NONE) {
                    if (!player.getScoreboardTags().contains("leaveFaction")) {
                        if (prsonPlayer.getLevel() >= 5) {
                            new FactionRandomMenu(prsonPlayer);
                        } else {
                            player.sendMessage("Чтобы получить доступ к данной команде, Вам нужно достичь §b5 §rуровня!");
                        }
                    } else {
                        new FactionChoseMenu(player);
                    }
                } else
                    player.sendMessage("§cВыбор фракции больше не возможен. Для выхода из фракции используйте §b/faction leave");
            } else if (args.length == 1 && args[0].equals("leave")) {
                if (prsonPlayer.getFaction() != Prison.Faction.NONE) {
                    if (prsonPlayer.getMoney() >= 5000) {
                        prsonPlayer.setFaction(Prison.Faction.NONE);
                        prsonPlayer.setMoney(prsonPlayer.getMoney() - 5000);
                        player.getScoreboardTags().add("leaveFaction");
                        player.sendMessage("§bПоздравляю! Вы вышли из фракции, теперь Вы можете присоеденится к любой фракции на Ваш выбор!  §c§l(/faction)");
                    } else {
                        player.sendMessage("§cУ Вас не достаточно денег для выхода из фракции! §b(Требуется 5000$)");
                    }
                } else
                    player.sendMessage("§bУ Вас уже нет фракции! Используйте комнду для выбора фракции! §c(/faction)");
            }
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}
