package ru.zencraft.prison.commands;

import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.menu.SellMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import static ru.zencraft.prison.Prison.dataFolder;

public class SellCommand extends Command {
    public static YamlConfiguration sellFile = YamlConfiguration.loadConfiguration(new File(dataFolder, "sell.yml"));
    public static Map<Material, Double> sellMap = Maps.newHashMap();

    public SellCommand() {
        super
                (
                        "sell",
                        "Повышение своего уровня",
                        "/sell",
                        new ArrayList<>()
                );
        sellFile.getKeys(false).forEach(key -> {
            Material material = Material.getMaterial(key.toUpperCase());
            if (material != null) {
                sellMap.put(material, sellFile.getDouble(key));
            }
        });
    }

    public static void sellItems(Inventory inv, PrisonPlayer prisonPlayer, boolean allowSell) {
        double totalMoney = 0;
        int totalItems = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack item = inv.getItem(i);
            if (item == null) continue;
            double sell = sellMap.getOrDefault(item.getType(), 0d);
            if (sell == 0) continue;
            int amount = item.getAmount();
            totalItems = totalItems + amount;
            totalMoney = totalMoney + (amount * sell);
            if (allowSell) item.setAmount(0);
        }
        if (totalMoney != 0) {
            if (allowSell) {
                prisonPlayer.setMoney(prisonPlayer.getMoney() + totalMoney);
                prisonPlayer.player.sendMessage("§a§l+ $" + PrisonPlayer.decimalFormat.format(totalMoney).replace(",", ".") + " §7[ §eПродано " + totalItems + " §eблоков §7]");

                double booster = Prison.calculateMoneyBooster(prisonPlayer);

                if (booster > 1) {
                    double boostedMoney = (totalMoney * booster) - totalMoney;
                    prisonPlayer.setMoney(prisonPlayer.getMoney() + boostedMoney);
                    prisonPlayer.player.sendMessage("§a§l+ $" + PrisonPlayer.decimalFormat.format(boostedMoney).replace(",", ".") + " §ex" + booster + " бустер");
                    prisonPlayer.player.sendMessage("§2ВСЕГО : §6+ $" + PrisonPlayer.decimalFormat.format(totalMoney + boostedMoney).replace(",", "."));
                }
            } else prisonPlayer.player.openInventory(new SellMenu(prisonPlayer.player).inventory);
        } else {
            prisonPlayer.player.sendMessage("§eУ вас нету блоков которые можно продать");
        }
    }

    public static void sellItem(ItemStack item, PrisonPlayer prisonPlayer) {
        double totalMoney = 0;

        if (item == null) return;
        double sell = sellMap.getOrDefault(item.getType(), 0d);
        if (sell == 0) return;
        int amount = item.getAmount();
        totalMoney = totalMoney + (amount * sell);
        item.setAmount(0);

        if (totalMoney != 0) {
            prisonPlayer.setMoney(prisonPlayer.getMoney() + totalMoney);
            double booster = Prison.calculateMoneyBooster(prisonPlayer);

            if (booster > 1) {
                double boostedMoney = (totalMoney * booster) - totalMoney;
                prisonPlayer.setMoney(prisonPlayer.getMoney() + boostedMoney);
            }
        }
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
            if (args.length == 0) {
                sellItems(player.getInventory(), prisonPlayer, false);
            } else {
                if (args[0].equals("all")) {
                    sellItems(player.getInventory(), prisonPlayer, true);
                } else {
                    player.sendMessage("§eВы используете не правильную команду для продажи. §b§l(/sell all)");
                }
            }
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }


}
