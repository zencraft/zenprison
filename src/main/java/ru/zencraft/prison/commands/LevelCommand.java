package ru.zencraft.prison.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.menu.LevelMenu;

import java.util.ArrayList;

import static ru.zencraft.prison.Prison.config;

public class LevelCommand extends Command {
    public LevelCommand() {
        super
                (
                        "level",
                        "Повышение своего уровня",
                        "/level",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            PrisonPlayer prsonPlayer = PrisonPlayer.get(player);
            if (prsonPlayer.getLevel() >= config.getInt("levels.maxLevel", 17)) {
                sender.sendMessage("§aВы достигли максимального уровня!");
            } else player.openInventory(new LevelMenu(prsonPlayer).inventory);
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}
