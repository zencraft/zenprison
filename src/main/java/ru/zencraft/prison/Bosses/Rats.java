package ru.zencraft.prison.Bosses;

import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;

import java.io.File;

import static ru.zencraft.prison.Prison.*;

public class Rats {
    public static YamlConfiguration cfg = YamlConfiguration.loadConfiguration(new File(dataFolder, "rats.yml"));

    public Rats() {
        cfg.getKeys(false).forEach(key -> {
            craftEntity("ZenPrisonRat_" + key + "#" + cfg.getString(key + ".type", "chicken"));
        });
    }

    public static void removeAll() {
        server.getWorlds().forEach(worlds -> {
            worlds.getEntities().forEach(entity -> {
                entity.getScoreboardTags().forEach(tags -> {
                    if (tags.startsWith("ZenPrisonRat")) {
                        entity.remove();
                    }
                });
            });
        });
    }

    public static LivingEntity craftEntity(String id) {
        String[] split = id.split("_")[1].split("#");
        Location location = new Location(world, cfg.getDouble(split[0] + ".x", 0), cfg.getDouble(split[0] + ".y", 80), cfg.getDouble(split[0] + ".z", 0));

        switch (split[1]) {
            case "silverfish": {
                Silverfish silverfish = world.spawn(location, Silverfish.class);
                silverfish.setCustomName("§aСкунс");
                silverfish.setCustomNameVisible(true);
                silverfish.getScoreboardTags().add(id);
                silverfish.setAI(false);
                silverfish.setGravity(true);
                return silverfish;
            }
            case "spider": {
                Spider spider = world.spawn(location, Spider.class);
                spider.setCustomName("§6Тарантул");
                spider.setCustomNameVisible(true);
                spider.getScoreboardTags().add(id);
                spider.setAI(false);
                spider.setGravity(true);
                return spider;
            }
            case "cavespider": {
                CaveSpider caveSpider = world.spawn(location, CaveSpider.class);
                caveSpider.setCustomName("§cКлещ");
                caveSpider.setCustomNameVisible(true);
                caveSpider.getScoreboardTags().add(id);
                caveSpider.setAI(false);
                caveSpider.setGravity(true);
                return caveSpider;
            }
            case "babyzombie":
            case "zombie": {
                Zombie zombie = world.spawn(location, Zombie.class);
                if (split[1].equals("zombie")) {
                    zombie.setBaby(false);
                    zombie.setCustomName("§eБомж");
                } else {
                    zombie.setBaby(true);
                    zombie.setCustomName("§3Цыган");
                }
                zombie.setCustomNameVisible(true);
                zombie.getScoreboardTags().add(id);
                zombie.setAI(false);
                zombie.setGravity(true);
                return zombie;
            }
            default: {
                Chicken chicken = (Chicken) world.spawnEntity(location, EntityType.CHICKEN);
                chicken.setCustomNameVisible(true);
                chicken.setCustomName("Ошибка чтения конфига. Неизвестный тип: " + split[1]);
                chicken.setGravity(true);
                return chicken;
            }
        }
    }
}
