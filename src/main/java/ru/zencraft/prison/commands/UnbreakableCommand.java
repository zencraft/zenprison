package ru.zencraft.prison.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.api.ItemBuilder;

import java.util.ArrayList;

public class UnbreakableCommand extends Command {
    public UnbreakableCommand() {
        super
                (
                        "unbreakable",
                        "Не разрушаемый предмет",
                        "/unbreakable",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            if (!sender.isOp()) return true;
            Player player = (Player) sender;
            ItemStack itemStack = player.getInventory().getItemInMainHand();
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                player.sendMessage("Возьмите предмет в руку");
                return true;
            }
            player.getInventory().setItemInMainHand(new ItemBuilder(itemStack).unbreakable(true).build());
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}