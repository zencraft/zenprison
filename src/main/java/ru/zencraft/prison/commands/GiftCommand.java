package ru.zencraft.prison.commands;

import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.api.ReflectionUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import static ru.zencraft.prison.Prison.server;

public class GiftCommand extends Command {
    public static Set<Player> waitToConfirm = new HashSet<>();

    public GiftCommand() {
        super
                (
                        "gift",
                        "Передача предметов игроку",
                        "/gift <ник>",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                sender.sendMessage(usageMessage);
                return true;
            }
            if (args.length == 1) {
                Player giftPlayer = server.getPlayer(args[0]);
                if (giftPlayer == null || !giftPlayer.isOnline()) {
                    sender.sendMessage("Игрок не онлайн");
                    return true;
                }
                ItemStack itemStack = player.getInventory().getItemInMainHand();
                if (itemStack == null || itemStack.getType() == Material.AIR) {
                    sender.sendMessage("Положите предмет в руку который хотите подарить");
                    return true;
                }
                String displayName = itemStack.getItemMeta().getDisplayName();
                BaseComponent[] main1 = new ComponentBuilder("§bВы хотите передать ").create();

                BaseComponent[] item = new ComponentBuilder("§c§l" + itemStack.getAmount() + " " + (displayName != null ? displayName : itemStack.getType().name()))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_ITEM, new ComponentBuilder(convertItemStackToJson(itemStack)).create()))
                        .create();
                BaseComponent[] main2 = new ComponentBuilder(" §bигроку §c§l" + giftPlayer.getName() + "§b?").create();
                BaseComponent[] yes = new ComponentBuilder("§7[ §aДа §7]")
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§aВы подтверждаете передачу предмета").create()))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/gift " + giftPlayer.getName() + " yes")).create();
                BaseComponent[] no = new ComponentBuilder("§7[ §cНет §7]")
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cВы не подтверждаете передачу предмета").create()))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/gift " + giftPlayer.getName() + " no")).create();
                TextComponent text = new TextComponent();
                text.addExtra(main1[0]);
                text.addExtra(item[0]);
                text.addExtra(main2[0]);
                text.addExtra("\n");
                text.addExtra(yes[0]);
                text.addExtra("   ");
                text.addExtra(no[0]);

                player.sendMessage(text);
                if (!waitToConfirm.contains(player)) waitToConfirm.add(player);
            }
            if (args.length == 2) {
                if (!waitToConfirm.contains(player)) return true;
                Player giftPlayer = server.getPlayer(args[0]);
                if (giftPlayer == null || !giftPlayer.isOnline()) {
                    sender.sendMessage("Игрок не онлайн");
                    return true;
                }
                ItemStack itemStack = player.getInventory().getItemInMainHand();
                if (itemStack == null || itemStack.getType() == Material.AIR) {
                    sender.sendMessage("Положите предмет в руку который хотите подарить");
                    return true;
                }
                switch (args[1]) {
                    case "yes": {
                        if (isInventoryFull(giftPlayer)) {
                            //§bУ §c§l%ник% §bполный инвентарь, попросите Его освободить место под Ваш предмет/ы
                            sender.sendMessage("§bУ §c§l" + giftPlayer.getName() + " §bполный инвентарь, попросите Его освободить место под Ваш" + (itemStack.getAmount() > 1 ? "и" : "") + " предмет" + (itemStack.getAmount() > 1 ? "ы" : ""));
                        } else {
                            giftPlayer.getInventory().addItem(itemStack.clone());
                            String displayName = itemStack.getItemMeta().getDisplayName();
                            int amount = itemStack.getAmount();
                            sender.sendMessage("§aПередача предмет" + (itemStack.getAmount() > 1 ? "ов" : "а") + " успешно выполнена!");

                            //§bВы получили §c§l%назваие_предмета% §bв количестве §c§l%количество% §bот игрока §c§l%ник%

                            BaseComponent[] text1 = new ComponentBuilder("§bВы получили ").create();
                            BaseComponent[] item = new ComponentBuilder("§c§l" + (displayName != null ? displayName : itemStack.getType().name()))
                                    .event(new HoverEvent(HoverEvent.Action.SHOW_ITEM, new ComponentBuilder(convertItemStackToJson(itemStack)).create()))
                                    .create();
                            BaseComponent[] text2 = new ComponentBuilder(" §bв количестве §c§l" + amount + " §bот игрока §c§l" + player.getName()).create();
                            TextComponent textComponent = new TextComponent(text1);
                            textComponent.addExtra(item[0]);
                            textComponent.addExtra(text2[0]);

                            giftPlayer.sendMessage(textComponent);
                            player.getInventory().getItemInMainHand().setAmount(0);
                        }
                        break;
                    }
                    case "no": {
                        sender.sendMessage("§cПередача предмет" + (itemStack.getAmount() > 1 ? "ов" : "а") + " отменена!");
                        break;
                    }
                }
                waitToConfirm.remove(player);
            }
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }

    public boolean isInventoryFull(Player player) {
        return player.getInventory().firstEmpty() == -1;
    }

    public String convertItemStackToJson(ItemStack itemStack) {
        // ItemStack methods to get a net.minecraft.server.ItemStack object for serialization
        Class<?> craftItemStackClazz = ReflectionUtil.getOBCClass("inventory.CraftItemStack");
        Method asNMSCopyMethod = ReflectionUtil.getMethod(craftItemStackClazz, "asNMSCopy", ItemStack.class);

        // NMS Method to serialize a net.minecraft.server.ItemStack to a valid Json string
        Class<?> nmsItemStackClazz = ReflectionUtil.getNMSClass("ItemStack");
        Class<?> nbtTagCompoundClazz = ReflectionUtil.getNMSClass("NBTTagCompound");
        Method saveNmsItemStackMethod = ReflectionUtil.getMethod(nmsItemStackClazz, "save", nbtTagCompoundClazz);

        Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
        Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method
        Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

        try {
            nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
            nmsItemStackObj = asNMSCopyMethod.invoke(null, itemStack);
            itemAsJsonObject = saveNmsItemStackMethod.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
        } catch (Throwable t) {
            Bukkit.getLogger().log(Level.SEVERE, "failed to serialize itemstack to nms item", t);
            return null;
        }

        // Return a string representation of the serialized object
        return itemAsJsonObject.toString();
    }
}
