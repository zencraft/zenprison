package ru.zencraft.prison.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.api.ItemBuilder;
import ru.zencraft.prison.menu.NPCShopMenu;

import java.util.ArrayList;
import java.util.List;

public class NpcCommand extends Command {
    private static String unknownCommand = "Unknown command. Type \"/help\" for help";

    public NpcCommand() {
        super
                (
                        "npc",
                        "Команда для редактирования NPC",
                        "/npc",
                        new ArrayList<>()
                );
    }

    public static Villager createNPC(Location location, String name, int type) {
        Villager villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);
        Villager.Profession[] professions = Villager.Profession.values();
        villager.setProfession(professions[type]);
        villager.setCanPickupItems(false);
        villager.setCustomName(name);
        villager.setCustomNameVisible(true);
        villager.setInvulnerable(true);
        villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 255, true));
        return villager;
    }

    public static ItemStack getPrice(ItemStack itemStack, double price) {
        List<String> lore = itemStack.getItemMeta().getLore();
        if (lore == null) lore = new ArrayList<>();
        ItemBuilder ib = new ItemBuilder(itemStack);
        lore.add(" ");
        lore.add("§6Цена: " + Prison.formatDouble(price) + "$");
        return ib.lore(lore).build();
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!player.isOp()) {
                sender.sendMessage("Нет прав");
                return true;
            }
            if (args.length == 0) {
                sender.sendMessage(unknownCommand);
                return true;
            }
            switch (args[0]) {
                case "create": {
                    if (args.length != 3) {
                        sender.sendMessage(unknownCommand);
                        return true;
                    }
                    String name = args[1].replace("&", "§").replace("_", " ");
                    createNPC(player.getLocation(), name, Integer.parseInt(args[2]));
                    new NPCShopMenu(args[1]);

                    break;
                }
                case "add": {
                    NPCShopMenu npcShopMenu = NPCShopMenu.menuMap.get(args[1]);
                    if (npcShopMenu == null) {
                        sender.sendMessage("NPC не найден");
                        return true;
                    }
                    ItemStack itemStack = player.getInventory().getItemInMainHand();
                    if (itemStack == null) {
                        sender.sendMessage("Положите предмет в руку");
                        return true;
                    }
                    String id = itemStack.getItemMeta().getLocalizedName();
                    String shopid = itemStack.getType().name() + (id != null ? "-" + id : "");
                    npcShopMenu.cfg.set(shopid, getPrice(itemStack, Double.parseDouble(args[2])));
                    npcShopMenu.initItems();
                    sender.sendMessage("Предмет добавлен");
                    break;
                }
            }

        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}
