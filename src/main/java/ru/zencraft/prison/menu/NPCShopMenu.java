package ru.zencraft.prison.menu;

import com.google.common.collect.Maps;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.zencraft.prison.PrisonPlayer;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.api.ItemBuilder;

import java.io.File;
import java.util.List;
import java.util.Map;

import static ru.zencraft.prison.Prison.*;

public class NPCShopMenu extends CustomMenu {
    public static ItemStack glass = new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 11)).displayName(" ").build();
    public static Map<String, NPCShopMenu> menuMap = Maps.newHashMap();

    public YamlConfiguration cfg;

    public NPCShopMenu(String id) {
        super(5, "Магазин");
        cfg = YamlConfiguration.loadConfiguration(new File(dataFolder, "shops/" + id + ".yml"));
        initItems();
        menuMap.put(id, this);
        world.getEntitiesByClass(Villager.class).forEach(villager -> {
            if (villager.getCustomName().replace("§", "&").replace(" ", "_").equals(id)) {
                Location npcLoc = villager.getLocation();
                server.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
                    Location loc = villager.getLocation();
                    npcLoc.setYaw(loc.getYaw());
                    npcLoc.setPitch(loc.getPitch());
                    villager.teleport(npcLoc);
                }, 0, 100);
            }
        });

    }

    public void initItems() {
        inventory.clear();
        for (int i = 0; i < inventory.getSize(); i++) {
            if (i >= 10 && i <= 16) continue;
            if (i >= 19 && i <= 25) continue;
            if (i >= 28 && i <= 34) continue;
            inventory.setItem(i, glass);
        }
        cfg.getKeys(false).forEach(key -> {
            inventory.addItem(cfg.getItemStack(key));
        });
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        event.setCancelled(true);
        if (event.getClickedInventory() == null || event.getClickedInventory().getHolder() != inventory.getHolder())
            return;
        ItemStack currentItem = event.getCurrentItem();
        if (currentItem.getType() == Material.AIR || currentItem.getType() == Material.STAINED_GLASS_PANE) return;
        Player player = (Player) event.getWhoClicked();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);

        double price = getPrice(currentItem);
        double money = prisonPlayer.getMoney();
        if (money >= price) {
            prisonPlayer.setMoney(money - price);
            player.getInventory().addItem(getItem(currentItem.clone()));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Integer.MAX_VALUE, 1);
        } else {
            player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, Integer.MAX_VALUE, 0.5f);
        }
    }

    public double getPrice(ItemStack itemStack) {
        List<String> lore = itemStack.getItemMeta().getLore();
        for (String line : lore) {
            if (line.startsWith("§6Цена: ")) {
                return Double.parseDouble(line.replace("§6Цена: ", "").replace("$", ""));
            }
        }
        return 0;
    }

    public ItemStack getItem(ItemStack itemStack) {
        List<String> lore = itemStack.getItemMeta().getLore();
        if (lore.size() >= 1) lore.remove(lore.size() - 1);
        if (lore.size() >= 2) lore.remove(lore.size() - 2);
        if (lore.isEmpty()) {
            ItemBuilder itemBuilder = new ItemBuilder(itemStack);
            itemBuilder.lore(null);
            return itemBuilder.build();
        } else {
            ItemBuilder itemBuilder = new ItemBuilder(itemStack);
            itemBuilder.lore(lore);
            return itemBuilder.build();
        }
    }
}
