package ru.zencraft.prison.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.Bosses.Boss;

import java.util.ArrayList;

public class BossCommand extends Command {
    public BossCommand() {
        super
                (
                        "boss",
                        "Информация о боссах",
                        "/boss",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.openInventory(Boss.bossMenu.inventory);
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}