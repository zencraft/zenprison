package ru.zencraft.prison.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.zencraft.prison.Prison;
import ru.zencraft.prison.PrisonPlayer;

import java.util.ArrayList;

import static ru.zencraft.prison.Prison.server;

public class PayCommand extends Command {
    public PayCommand() {
        super
                (
                        "pay",
                        "Отправить деньги игроку",
                        "/pay <ник> <сумма>",
                        new ArrayList<>()
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player playerSender = (Player) sender;
            if (args.length != 2) {
                sender.sendMessage(usageMessage);
                return true;
            }
            Player playerGetter = server.getPlayer(args[0]);
            if (playerGetter == null || !playerGetter.isOnline()) {
                sender.sendMessage("Игрок не онлайн");
                return true;
            }
            double sendMoney = 0;
            try {
                sendMoney = Double.parseDouble(args[1].replace("-", ""));
            } catch (NumberFormatException ex) {
                sender.sendMessage("Вы ввели не правильное число");
            }
            PrisonPlayer prisonPlayer = PrisonPlayer.get(playerSender);
            if (prisonPlayer.getMoney() < sendMoney) {
                sender.sendMessage("У вас не достаточно денег");
                return true;
            }
            PrisonPlayer prisonGetter = PrisonPlayer.get(playerGetter);
            prisonGetter.setMoney(prisonGetter.getMoney() + sendMoney);

            prisonPlayer.setMoney(prisonPlayer.getMoney() - sendMoney);

            sender.sendMessage("§aВы отправили " + Prison.formatDouble(sendMoney) + "$ игроку " + prisonGetter.player.getName());
            prisonGetter.player.sendMessage("§aВы получили " + Prison.formatDouble(sendMoney) + "$ от игрока " + playerSender.getName());
        } else {
            sender.sendMessage("Эту команду можно выполнять только в игре");
        }
        return true;
    }
}