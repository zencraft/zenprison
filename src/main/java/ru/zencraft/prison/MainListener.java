package ru.zencraft.prison;

import com.destroystokyo.paper.event.server.ServerExceptionEvent;
import com.google.common.collect.Maps;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import ru.zencraft.prison.Bosses.Boss;
import ru.zencraft.prison.Bosses.Rats;
import ru.zencraft.prison.api.ChatUtils;
import ru.zencraft.prison.api.CustomMenu;
import ru.zencraft.prison.commands.SellCommand;
import ru.zencraft.prison.menu.NPCShopMenu;
import ru.zencraft.prison.menu.SellMenu;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static ru.zencraft.prison.Prison.*;

public class MainListener implements Listener {
    public static Player latestWhoBlockBreak = null;
    public static Location latestBlockBreak = null;
    public static Set<Material> canBreak = new HashSet<>();

    public MainListener() {
        SellCommand.sellFile.getKeys(false).forEach(key -> {
            Material material = Material.valueOf(key);
            canBreak.add(material);
        });
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        InventoryHolder inventoryHolder = event.getInventory().getHolder();
        if (inventoryHolder instanceof CustomMenu)
            ((CustomMenu) inventoryHolder).onClick(event);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        InventoryHolder inventoryHolder = event.getInventory().getHolder();
        Player player = (Player) event.getPlayer();
        if (inventoryHolder instanceof SellMenu) {
            SellMenu sellMenu = (SellMenu) inventoryHolder;
            SellCommand.sellItems(sellMenu.inventory, PrisonPlayer.get(player), true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PrisonPlayer prisonPlayer = new PrisonPlayer(player);

        player.setPlayerListName(
                prisonPlayer.getFaction() != Prison.Faction.NONE ?
                        (prisonPlayer.getFaction() == Prison.Faction.BLUE ? "§3" + player.getName() : "§4" + player.getName())
                        : "§7" + player.getName());

        String prefix = PermissionsEx.getUser(player).getPrefix(null);
        player.setDisplayName(prefix + player.getName());
        player.getEffectivePermissions().forEach(set -> {
            String perm = set.getPermission();
            if (!perm.startsWith("zenprison")) return;
            if (perm.startsWith("zenprison.booster.money")) {
                String[] split = perm.split("zenprison.booster.money.");
                try {
                    prisonPlayer.setMoneyBooster(Double.parseDouble(split[1]));
                } catch (NumberFormatException ex) {
                    System.out.println("Не правильное число в permission " + perm + " у игрока " + player.getName());
                }
            }
            if (perm.startsWith("zenprison.booster.block")) {
                String[] split = perm.split("zenprison.booster.block.");
                try {
                    prisonPlayer.setBlockBooster(Integer.parseInt(split[1]));
                } catch (NumberFormatException ex) {
                    System.out.println("Не правильное число в permission " + perm + " у игрока " + player.getName());
                }
            }
        });
        if (!player.hasPlayedBefore()) {
            PlayerInventory inv = player.getInventory();
            inv.addItem(Items.itemMap.get("axe_1"));
            inv.addItem(Items.apple);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        prisonPlayer.unload();
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (event.isCancelled()) return;
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if (!canBreak.contains(block.getType()) && player.getGameMode() != GameMode.CREATIVE) {
            event.setCancelled(true);
            return;
        }
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        int booster = Prison.calculateBlockBooster(prisonPlayer);
        prisonPlayer.setBlocksMined(prisonPlayer.getBlocksMined() + (booster));
        prisonPlayer.addTotalMaterialBlockMined(block.getType(), (booster));
        latestWhoBlockBreak = player;
        latestBlockBreak = block.getLocation();
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);

        prisonPlayer.setDeaths(prisonPlayer.getDeaths() + 1);
        event.setKeepInventory(true);
        PrisonPlayer.calculateDrops(player);

        Player killer = player.getKiller();
        if (killer != null) {
            PrisonPlayer prisonKiller = PrisonPlayer.get(killer);
            prisonKiller.setKills(prisonKiller.getKills() + 1);

            double playerMoney = prisonPlayer.getMoney();
            double loseMoney = playerMoney / 100;

            prisonPlayer.setMoney(playerMoney - loseMoney);
            prisonKiller.setMoney(prisonKiller.getMoney() + loseMoney);

            String loseMoneyString = PrisonPlayer.decimalFormat.format(loseMoney).replace(",", ".");

            player.sendMessage("§bВы были убиты §c§l" + killer.getName() + " §bи потеряли §c§l" + loseMoneyString + "$");
            killer.sendMessage("§bВы убили §c§l" + player.getName() + " §bи получили §c§l" + loseMoneyString + "$");
        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onException(ServerExceptionEvent event) {
        Exception ex = event.getException();
        String message = "Ошибка:§7 " + ex.getLocalizedMessage();
        server.getOnlinePlayers().forEach(players -> {
            players.sendActionBar(message);
            if (players.isOp()) {
                players.sendMessage(message);
                players.playSound(players.getLocation(), Sound.BLOCK_ANVIL_USE, Integer.MAX_VALUE, 2);
            }
        });
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();
        if (message.startsWith("/sellall")) {
            event.setCancelled(true);
            player.sendMessage("§eВы используете не правильную команду для продажи. §b§l(/sell all)");
        }
        if (message.startsWith("/itemslist")) {
            if (player.isOp()) {
                event.setCancelled(true);
                Inventory inventory = server.createInventory(null, 9 * 16, "");
                Items.itemsCfg.getKeys(false).forEach(key -> inventory.addItem(Items.itemMap.get(key)));
                player.openInventory(inventory);
            }
        }
    }

    @EventHandler
    public void onDamageByPlayer(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        Entity damager = event.getDamager();
        if (damager.getType() == EntityType.PLAYER) {
            Player player = (Player) damager;
            entity.getScoreboardTags().forEach(tag -> {
                if (tag.startsWith("ZenPrison_boss#")) {
                    String[] split = tag.split("#");
                    String id = split[1];
                    Boss boss = Boss.bossMap.get(id);
                    double bosshelth = ((LivingEntity) entity).getHealth();
                    server.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                        boss.damageMap.put(player, boss.damageMap.getOrDefault(damager, 0d) + event.getDamage());
                    });
                    Boss.BossAbility[] abilities = Boss.BossAbility.values();
                    if (Prison.random.nextInt(100) <= 25)
                        boss.runAbility(player, abilities[random.nextInt(abilities.length)]);
                }
                if (tag.startsWith("ZenPrisonRat_")) {
                    ((LivingEntity) entity).setAI(true);
                    server.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                        if (entity.isDead()) {
                            PrisonPlayer prisonPlayer = PrisonPlayer.get((Player) damager);
                            prisonPlayer.setMoney(prisonPlayer.getMoney() + 0.25);
                            prisonPlayer.player.sendMessage("§aНа Ваш счёт было зачисленно §20.25$");
                            Rats.craftEntity(tag);
                        }
                    });
                }
            });
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        entity.getScoreboardTags().forEach(tag -> {
            if (tag.startsWith("ZenPrison_boss#")) {
                if (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                    event.setCancelled(true);
                }
            }
        });
    }


    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        Entity entity = event.getEntity();
        entity.getScoreboardTags().forEach(tag -> {
            if (tag.startsWith("ZenPrison_boss#")) {
                String[] split = tag.split("#");
                String id = split[1];
                Boss boss = Boss.bossMap.get(id);
                server.broadcastMessage("§cБосс §b" + entity.getCustomName() + " §cбыл повержен! И нападавшие получили вознаграждение за убийство босса!");
                final double[] givedCash = {0};
                Map<Player, Double> totalCash = Maps.newHashMap();
                boss.damageMap.forEach((player, damage) -> {
                    if (!player.isOnline()) return;
                    double cash = boss.bank / 100 * (100 / boss.health * damage);
                    givedCash[0] = givedCash[0] + cash;
                    totalCash.put(player, totalCash.getOrDefault(player, 0d) + cash);
                });

                double giftCash = (boss.bank - givedCash[0]) / totalCash.size();

                boss.damageMap.forEach((player, damage) -> {
                    if (!player.isOnline()) return;
                    double cash = totalCash.get(player) + giftCash;
                    PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
                    prisonPlayer.setMoney(prisonPlayer.getMoney() + cash);
                    player.sendMessage("§cПоздравляю! Вы победили §b" + entity.getCustomName() + " §cи получили §b" + Prison.formatDouble(cash) + "$§c!");
                });
                totalCash.clear();
            }
        });
    }


    @EventHandler
    public void summon(ItemSpawnEvent event) {
        Item itementity = event.getEntity();
        Location enloc = itementity.getLocation();
        ItemStack item = itementity.getItemStack();
        Location location = new Location(enloc.getWorld(), enloc.getBlockX(), enloc.getBlockY(), enloc.getBlockZ());

        if (latestBlockBreak != null && equalsBlockLocation(latestBlockBreak, location) && latestWhoBlockBreak.getInventory().firstEmpty() != -1) {
            event.setCancelled(true);
            PrisonPlayer prisonPlayer = PrisonPlayer.get(latestWhoBlockBreak);
            if (prisonPlayer.autoSell) {
                SellCommand.sellItem(item, prisonPlayer);
            } else prisonPlayer.player.getInventory().addItem(item);
            latestWhoBlockBreak.playSound(latestWhoBlockBreak.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
        }
        latestBlockBreak = null;
        latestWhoBlockBreak = null;
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        Item item = event.getItemDrop();
        ItemStack itemStack = item.getItemStack();
        if (itemStack.getItemMeta().getLore() != null && !itemStack.getItemMeta().getLore().isEmpty()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();
        if (inventory.getType() == InventoryType.MERCHANT) event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        Entity entity = event.getRightClicked();
        Player player = event.getPlayer();
        if (entity.getType() == EntityType.VILLAGER) {
            String name = entity.getCustomName();
            if (name != null) {
                NPCShopMenu npcShopMenu = NPCShopMenu.menuMap.get(name.replace("§", "&").replace(" ", "_"));
                if (npcShopMenu != null) player.openInventory(npcShopMenu.inventory);
                // if (npcShopMenu != null) server.getScheduler().scheduleSyncDelayedTask(plugin,()->player.openInventory(npcShopMenu.inventory),5);
            }
        }
    }

    @EventHandler
    public void asyncChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        Player player = event.getPlayer();
        PrisonPlayer prisonPlayer = PrisonPlayer.get(player);
        String msg = event.getMessage();
        BaseComponent message = ChatUtils.generateChat(player, msg);
        if (message != null) {
            if (msg.startsWith("!")) {
                BaseComponent text = new TextComponent("§cⒼ ");
                text.addExtra(message);
                server.getOnlinePlayers().forEach(players -> players.sendMessage(text));
            } else {
                BaseComponent text = new TextComponent("§bⓁ ");
                text.addExtra(message);
                ChatUtils.getPlayersInRange(100, player.getLocation()).forEach(players -> players.sendMessage(text));
            }
            System.out.println(message.toPlainText());
        }
    }

    public boolean equalsBlockLocation(Location loc1, Location loc2) {
        if (loc1.getBlockY() == loc2.getBlockY()) {
            if (loc1.getBlockX() == loc2.getBlockX()) {
                return loc1.getBlockZ() == loc2.getBlockZ();
            } else return false;
        } else return false;
    }
}
