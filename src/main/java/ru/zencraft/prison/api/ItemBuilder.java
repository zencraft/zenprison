package ru.zencraft.prison.api;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

public class ItemBuilder {
    private final ItemStack item;
    private final ItemMeta meta;

    public ItemBuilder(ItemStack item) {
        this.item = item;
        meta = item.getItemMeta();
    }

    public ItemBuilder(Material material) {
        this.item = new ItemStack(material);
        meta = this.item.getItemMeta();
    }

    public ItemBuilder displayName(String name) {
        meta.setDisplayName("§f" + name);
        return this;
    }

    public ItemBuilder localizedName(String localizedName) {
        meta.setLocalizedName(localizedName);
        return this;
    }

    public ItemBuilder lore(List<String> lore) {
        meta.setLore(lore);
        return this;
    }

    public ItemBuilder enchant(Enchantment enchantment, int level) {
        return enchant(enchantment, level, true);
    }

    public ItemBuilder enchant(Enchantment enchantment, int level, boolean show) {
        meta.addEnchant(enchantment, level, show);
        return this;
    }

    public ItemBuilder itemFlag(ItemFlag... flags) {
        meta.addItemFlags(flags);
        return this;
    }

    public ItemBuilder unbreakable(boolean unbreakable) {
        meta.setUnbreakable(unbreakable);
        return this;
    }

    public ItemBuilder head(String name) {
        if (this.item.getType() != Material.SKULL_ITEM) return this;
        SkullMeta meta = (SkullMeta) this.meta;
        meta.setOwner(name);
        return this;
    }

    public ItemStack build() {
        item.setItemMeta(meta);
        return item;
    }
}
